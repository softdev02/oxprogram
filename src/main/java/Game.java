/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.noprada.oxoop;

import java.util.Scanner;

/**
 *
 * @author 66945
 */
public class Game {
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row,col;
    Scanner kb =new Scanner(System.in);
    
    public Game() {
         playerX = new Player('X');
         playerO = new Player('O');
        table = new Table(playerX,playerO);
   
    }
    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }
    public void showTable(){
        table.showTable();
    }
    public void input(){
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
//            System.out.println("row: "+row +" col: "+col);//bebug
//            if (table[row][col] == '-') {
//                table[row][col] = Player;
//                break;
//            }
        if(table.setRowCol(row, col)){
            break;
        }
            System.out.println("Error: table at row and col is not empty!!");
        }
    }
    public void run() {
        this.showWelcome();
        this.showTable();
        this.input();
        
    }
}
