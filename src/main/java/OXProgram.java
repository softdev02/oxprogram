/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Scanner;

/**
 *
 * @author 66945
 */
public class OXProgram {

    static boolean isFinish = false;
    static char winner ='-';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char Player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game ");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(Player + " turn");
    }

    static void Input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
//            System.out.println("row: "+row +" col: "+col);//bebug
            if (table[row][col] == '-') {
                table[row][col] = Player;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!");
        }

    }
    static void checkCol() {
        for(int row=0;row<3;row++){
            if(table[row][col] != Player){
                return;
            }
        }
        isFinish =true;
        winner= Player;
    }
    static void checkRow() {
        for(int col=0;col<3;col++){
            if(table[row][col] != Player){
                return;
            }
        }
        isFinish =true;
        winner= Player;
    }
    static void checkWin() {
        checkRow();
        checkCol();
        checkX();
    }
    static void checkX(){
        
    }
    static void checkDraw(){
        
    }
    static void switchPlayer() {
        if (Player == 'X') {
            Player = 'O';
        } else {
            Player = 'X';
        }
    }

    static void showResult() {
        if(winner=='-'){
            System.out.println("Draw!!");
        }else{
            System.out.println(winner + " Win!!");
        }
        System.out.println("I don't known");
    }

    static void showBye() {
        System.out.println("Bye Bye....");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            Input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }
}
