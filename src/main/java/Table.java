/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.noprada.oxoop;

/**
 *
 * @author 66945
 */
public class Table {
    char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    Player playerX;
    Player playerO;
    Player currentPlayer;
    
    public Table(Player X, Player O){
        playerX = X;
        playerO = O; 
        currentPlayer = X;
    }
    public void showTable(){
        System.out.println(" 1 2 3");
        for(int i =0; i<table.length; i++){
            System.out.print(i + " ");
            for(int j =0; j<table[i].length; j++){
                System.out.print(table[i][j] + " ");
        }
            System.out.println("");
        }
    }
    public boolean setRowCol(int row,int col){
         if (table[row][col] == '-') {
              table[row][col] = currentPlayer.getName();
              return true;
            }
         return false;
    }
}
