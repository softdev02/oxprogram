/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.noprada.oxoop;

/**
 *
 * @author 66945
 */
public class Player {
    private char name;
    private int win;
    private int lode;
    private int draw;

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getLode() {
        return lode;
    }

    public void setLode(int lode) {
        this.lode = lode;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }
    
    public Player(char name){
        this.name = name;
    }
}
